package org.vaadin.virkki.paperstack;

import com.vaadin.Application;
import com.vaadin.ui.DateField;
import com.vaadin.ui.InlineDateField;
import com.vaadin.ui.Label;
import com.vaadin.ui.Window;

public class PaperstackApplication extends Application {

	@Override
	public void init() {
		Window mainWindow = new Window("Paperstack Application");
		setMainWindow(mainWindow);

		PaperStack paperStack = new PaperStack();

		paperStack.addComponent(new Label("Hello!"));

		InlineDateField inlineDateField = new InlineDateField();
		inlineDateField.setResolution(DateField.RESOLUTION_DAY);
		paperStack.addComponent(inlineDateField, "#999");

		mainWindow.addComponent(paperStack);

	}

}
