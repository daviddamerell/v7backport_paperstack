package org.vaadin.virkki.paperstack;

import java.io.Serializable;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import org.vaadin.virkki.paperstack.client.ui.VPaperStack;

import com.vaadin.terminal.PaintException;
import com.vaadin.terminal.PaintTarget;
import com.vaadin.ui.AbstractComponentContainer;
import com.vaadin.ui.ClientWidget;
import com.vaadin.ui.Component;

/**
 * PaperStack is a component container whose subcomponents are presented
 * sequentially, one subcomponent at a time. User can switch between the
 * subcomponents by mouse dragging the upper right corner of a view revealing
 * the underlying subcomponent simultaneously. The transition effect simulates
 * leafing through a stack of papers.
 * 
 * @author Tomi Virkki / Vaadin Ltd
 * 
 */
@ClientWidget(VPaperStack.class)
public class PaperStack extends AbstractComponentContainer {
	private static final long serialVersionUID = -7892431562611019574L;
	protected static final String DEFAULT_BACKGROUND_COLOR = "#fff";
	protected static final Pattern hexPattern = Pattern.compile("^#([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$");

	protected final List<Component> components = new ArrayList<Component>();
	protected final Map<Component, String> backgroundColors = new LinkedHashMap<Component, String>();

	protected int currentComponentIndex = 0;

	protected Integer componentRequested = null;
	protected int cloneElementCount = 7;
	protected String paperBackColor = "#00B4F0";
	protected String paperEdgeColor = "#00B4F0";

	protected boolean padding = true;
	protected Boolean navigate;
	protected boolean navigationComplete = true;
	
	protected double initialCanvasCornerX = 5.0;
	protected double initialCanvasCornerY = 10.0;
	protected double initialCanvasHeight = 35.0;
	protected double initialCanvasWidth = 25.0;

	public PaperStack() {
		setWidth(600, UNITS_PIXELS);
		setHeight(400, UNITS_PIXELS);
	}

	@Override
	public void paintContent(PaintTarget target) throws PaintException {
		if (navigate != null) {
			target.addAttribute(VPaperStack.NAVIGATE_IDENTIFIER, navigate);
			navigate = null;
		} else {
			List<Integer> indexes = Arrays.asList(componentRequested);
			if (componentRequested == null) {
				target.addAttribute(VPaperStack.CLONE_ELEMENT_COUNT_IDENTIFIER, cloneElementCount);
				target.addAttribute(VPaperStack.PAPER_BACK_COLOR_IDENTIFIER, paperBackColor);
				target.addAttribute(VPaperStack.PAPER_EDGE_COLOR_IDENTIFIER, paperEdgeColor);
				target.addAttribute(VPaperStack.PADDING_IDENTIFIER, padding);
				
				
				target.addAttribute(VPaperStack.PAPER_INITIAL_CORNER_X, initialCanvasCornerX);
				target.addAttribute(VPaperStack.PAPER_INITIAL_CORNER_Y, initialCanvasCornerY);
				target.addAttribute(VPaperStack.PAPER_INITIAL_HEIGHT, initialCanvasHeight);
				target.addAttribute(VPaperStack.PAPER_INITIAL_WIDTH, initialCanvasWidth);
				
				indexes = Arrays.asList(VPaperStack.CURRENT_COMPONENT_IDENTIFIER, VPaperStack.NEXT_COMPONENT_IDENTIFIER,
						VPaperStack.PREVIOUS_COMPONENT_IDENTIFIER);
			}
			componentRequested = null;

			for (int index : indexes) {
				if (components.size() > currentComponentIndex + index && 0 <= currentComponentIndex + index) {
					final Component component = components.get(currentComponentIndex + index);
					final String backgroundColor = backgroundColors.get(component);

					target.startTag(VPaperStack.COMPONENT_TAGNAME + index);
					component.paint(target);
					target.addAttribute(VPaperStack.COMPONENT_BACKGROUND_COLOR_IDENTIFIER, backgroundColor != null ? backgroundColor : DEFAULT_BACKGROUND_COLOR);
					target.endTag(VPaperStack.COMPONENT_TAGNAME + index);
				}
			}
		}
	}

	@Override
	public void changeVariables(Object source, Map<String, Object> variables) {
		super.changeVariables(source, variables);
		if (variables.containsKey(VPaperStack.PAGECHANGE_EVENT_IDENTIFIER)) {
			componentRequested = (Integer) variables.get(VPaperStack.PAGECHANGE_EVENT_IDENTIFIER);
			navigationComplete = true;
			if (componentRequested != VPaperStack.CURRENT_COMPONENT_IDENTIFIER) {
				currentComponentIndex += componentRequested;
				fireEvent(new PageChangeEvent(this));
				if (components.size() > currentComponentIndex + componentRequested && 0 <= currentComponentIndex + componentRequested) {
					requestRepaint();
				} else {
					componentRequested = null;
				}
			} else {
				componentRequested = null;
			}
		}
	}

	public double getInitialCanvasCornerX() {
		return initialCanvasCornerX;
	}

	public void setInitialCanvasCornerX(double initialCanvasCornerX) {
		this.initialCanvasCornerX = initialCanvasCornerX;
		
		requestRepaint();
	}

	public double getInitialCanvasCornerY() {
		return initialCanvasCornerY;
	}

	public void setInitialCanvasCornerY(double initialCanvasCornerY) {
		this.initialCanvasCornerY = initialCanvasCornerY;
		requestRepaint();
	}

	public double getInitialCanvasHeight() {
		return initialCanvasHeight;
	}

	public void setInitialCanvasHeight(double initialCanvasHeight) {
		this.initialCanvasHeight = initialCanvasHeight;
		requestRepaint();
	}

	public double getInitialCanvaswidth() {
		return initialCanvasWidth;
	}

	public void setInitialCanvaswidth(double initialCanvaswidth) {
		this.initialCanvasWidth = initialCanvaswidth;
		
		requestRepaint();
	}
	
	@Override
	public Iterator<Component> getComponentIterator() {
		return java.util.Collections.unmodifiableList(components).iterator();
	}

	/**
	 * Returns the component currently visible.
	 * 
	 * @return the current component.
	 */
	public Component getCurrentComponent() {
		if (currentComponentIndex >= 0 && currentComponentIndex < components.size()) {
			return components.get(currentComponentIndex);
		} else {
			return null;
		}
	}

	@Override
	public void addComponent(final Component c) {
		components.add(c);
		super.addComponent(c);
	}

	/**
	 * Add a component to a specified position
	 * 
	 * @param c
	 *            the component to be added.
	 * @param index
	 *            The index the component is added to.
	 */
	protected void addComponent(Component c, int index) {
		components.add(index, c);
		super.addComponent(c);
	}

	/**
	 * Adds a component with a pre-specified background color. The color must
	 * comply with hexadecimal notation to have effect.
	 * 
	 * @param c
	 *            the component to be added.
	 * @param backgroundColorHex
	 *            the background color of the component.
	 */
	public void addComponent(Component c, String backgroundColorHex) {
		if (c != null) {
			addComponent(c);
			if (backgroundColorHex != null && hexPattern.matcher(backgroundColorHex).matches()) {
				backgroundColors.put(c, backgroundColorHex);
			}
		}
	}

	@Override
	public void removeComponent(Component c) {
		if (c != null && components.contains(c)) {
			super.removeComponent(c);
			if (components.indexOf(c) < currentComponentIndex) {
				currentComponentIndex--;
			} else if (components.indexOf(c) < currentComponentIndex + 2) {
				requestRepaint();
			}
			components.remove(c);
			backgroundColors.remove(c);
		}
	}

	@Override
	public void replaceComponent(Component oldComponent, Component newComponent) {
		// Source from com.vaadin.ui.TabSheet.java
		int oldLocation = -1;
		int newLocation = -1;
		int location = 0;
		for (final Iterator<Component> i = components.iterator(); i.hasNext();) {
			final Component component = i.next();

			if (component == oldComponent) {
				oldLocation = location;
			}
			if (component == newComponent) {
				newLocation = location;
			}

			location++;
		}

		if (oldLocation == -1) {
			addComponent(newComponent);
		} else if (newLocation == -1) {
			removeComponent(oldComponent);
			addComponent(newComponent, oldLocation);
		} else {
			if (oldLocation > newLocation) {
				components.remove(oldComponent);
				components.add(newLocation, oldComponent);
				components.remove(newComponent);
				components.add(oldLocation, newComponent);
			} else {
				components.remove(newComponent);
				components.add(oldLocation, newComponent);
				components.remove(oldComponent);
				components.add(newLocation, oldComponent);
			}

			requestRepaint();
		}
	}

	/**
	 * Sets the colors for the paper and the paper's edge. The colors must
	 * comply with hexadecimal notation to have effect.
	 * 
	 * @param paperBackColorHex
	 *            color of the back side of the paper.
	 * @param paperEdgeColorHex
	 *            color of the paper edge.
	 * 
	 */
	public void setPaperColor(String paperBackColorHex, String paperEdgeColorHex) {
		if (paperBackColorHex != null && hexPattern.matcher(paperBackColorHex).matches()) {
			this.paperBackColor = paperBackColorHex;
		}
		if (paperEdgeColorHex != null && hexPattern.matcher(paperEdgeColorHex).matches()) {
			this.paperEdgeColor = paperEdgeColorHex;
		}
		requestRepaint();
	}

	/**
	 * Defines the number of "clone elements" (a client side member) that are
	 * positioned in a certain way to simulate the (diagonal) borderline of two
	 * subcomponents. Increasing the count results in more natural appearance
	 * but reduces performance.
	 * 
	 * @param cloneElementCount
	 */
	public void setCloneElementCount(int cloneElementCount) {
		if (cloneElementCount >= 4) {
			this.cloneElementCount = cloneElementCount;
			requestRepaint();
		}
	}

	/**
	 * An event received by PageChangeListeners when the visible subcomponent is
	 * changed to the next one.
	 */
	public class PageChangeEvent extends Event {
		private static final long serialVersionUID = 6834729362872065434L;

		public PageChangeEvent(Component source) {
			super(source);
		}

		public PaperStack getPaperStack() {
			return (PaperStack) getSource();
		}
	}

	/**
	 * A listener that receives PageChangeEvents.
	 */
	public interface PageChangeListener extends Serializable {
		public void pageChange(PageChangeEvent event);
	}

	/**
	 * Adds a PageChangeListener to this PaperStack.
	 * 
	 * @param listener
	 *            the PageChangeListener to be added.
	 */
	public void addListener(PageChangeListener listener) {
		addListener(PageChangeEvent.class, listener, PAGE_CHANGE_METHOD);
	}

	private static final Method PAGE_CHANGE_METHOD;
	static {
		try {
			PAGE_CHANGE_METHOD = PageChangeListener.class.getDeclaredMethod("pageChange", new Class[] { PageChangeEvent.class });
		} catch (final java.lang.NoSuchMethodException e) {
			// This should never happen
			throw new java.lang.RuntimeException("Internal error finding methods in PaperStack");
		}
	}

	/**
	 * Does the PaperStack have padding.
	 * 
	 * @return
	 */
	public boolean isPadding() {
		return padding;
	}

	/**
	 * Set PaperStack padding on/off.
	 * 
	 * @param padding
	 */
	public void setPadding(boolean padding) {
		this.padding = padding;
		requestRepaint();
	}

	/**
	 * Navigates the views forward/backward.
	 * 
	 * @param forward
	 */
	public void navigate(boolean forward) {
		if ((forward && components.size() > currentComponentIndex + 1) || (!forward && currentComponentIndex > 0)) {
			if (navigationComplete) {
				navigationComplete = false;
				navigate = forward;
				requestRepaint();
			}
		}
	}
	
	public void setCurrentComponent(int index){
		setCurrentComponent(components.get(index));
	}

	/**
	 * Sets the currently visible component.
	 * 
	 * @param component
	 */
	public void setCurrentComponent(Component component) {
		if (components.contains(component)) {
			currentComponentIndex = components.indexOf(component);
			requestRepaint();
		}
	}
}
