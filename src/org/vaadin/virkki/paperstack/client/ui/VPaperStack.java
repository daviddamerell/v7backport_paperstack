package org.vaadin.virkki.paperstack.client.ui;

import java.util.Arrays;
import java.util.Date;
import java.util.Set;

import org.vaadin.virkki.paperstack.client.canvas.client.Canvas;

import com.google.gwt.animation.client.Animation;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.Style;
import com.google.gwt.dom.client.Style.Overflow;
import com.google.gwt.dom.client.Style.Position;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.dom.client.Style.Visibility;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;
import com.vaadin.terminal.gwt.client.ApplicationConnection;
import com.vaadin.terminal.gwt.client.BrowserInfo;
import com.vaadin.terminal.gwt.client.Container;
import com.vaadin.terminal.gwt.client.Paintable;
import com.vaadin.terminal.gwt.client.RenderSpace;
import com.vaadin.terminal.gwt.client.UIDL;

/**
 * Client side widget for the PaperStack component.
 * 
 * @author Tomi Virkki / Vaadin Ltd
 */
public final class VPaperStack extends Composite implements Paintable, Container {
	public static final String CLASSNAME = "v-paperstack";
	private String paintableId;
	private ApplicationConnection client;

	private final FlowPanel rootPanel = new FlowPanel();

	private double componentWidth;
	private double componentHeight;

	private PaintableWrapper previousComponent;
	private PaintableWrapper currentComponent;
	private PaintableWrapper nextComponent;

	private Image backButton = new Image(GWT.getModuleBaseURL() + "paperstack/backarrow.png");
	private final PaperStackCanvas canvas = new PaperStackCanvas();

	private final boolean isIE = BrowserInfo.get().isIE();

	public static final double PADDING_WIDTH = 20.0;
	private boolean padding;

	public static final String PAGECHANGE_EVENT_IDENTIFIER = "pc";
	public static final String CLONE_ELEMENT_COUNT_IDENTIFIER = "cec";
	public static final String PAPER_BACK_COLOR_IDENTIFIER = "pbc";
	public static final String PAPER_EDGE_COLOR_IDENTIFIER = "pec";
	public static final String PADDING_IDENTIFIER = "p";
	public static final String COMPONENT_TAGNAME = "c";
	public static final int PREVIOUS_COMPONENT_IDENTIFIER = -1;
	public static final int CURRENT_COMPONENT_IDENTIFIER = 0;
	public static final int NEXT_COMPONENT_IDENTIFIER = 1;
	public static final String COMPONENT_BACKGROUND_COLOR_IDENTIFIER = "cbc";
	public static final String NAVIGATE_IDENTIFIER = "n";
	
	public static final String PAPER_INITIAL_CORNER_X = "picx";
	public static final String PAPER_INITIAL_CORNER_Y = "picy";
	public static final String PAPER_INITIAL_HEIGHT = "pih";
	public static final String PAPER_INITIAL_WIDTH = "piw";
	
	
	public VPaperStack() {
		super();
		rootPanel.setStyleName(CLASSNAME);
		initWidget(rootPanel);
		getElement().getStyle().setProperty("position", "relative");

		Style backButtonStyle = backButton.getElement().getStyle();
		backButtonStyle.setProperty("position", "absolute");
		backButtonStyle.setProperty("bottom", "0");
		backButtonStyle.setProperty("cursor", "pointer");
		backButton.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				canvas.navigate(false);
			}
		});
	}

	@Override
	public void setWidth(final String width) {
		super.setWidth(width);
		componentWidth = getOffsetWidth();
		sizeChanged();
	}

	@Override
	public void setHeight(final String height) {
		super.setHeight(height);
		componentHeight = getOffsetHeight();
		sizeChanged();
	}

	/**
	 * Refreshes the PaintableWrappers' sizes.
	 */
	private void sizeChanged() {
		if (componentHeight > 0.0 && componentWidth > 0.0) {
			for (PaintableWrapper component : Arrays.asList(currentComponent, nextComponent, previousComponent)) {
				if (component != null) {
					component.refreshSize();
				}
			}
		}
	}

	public void updateFromUIDL(UIDL uidl, ApplicationConnection client) {
		if (client.updateComponent(this, uidl, true)) {
			return;
		}
		this.client = client;
		paintableId = uidl.getId();

		if (uidl.hasAttribute(NAVIGATE_IDENTIFIER)) {
			canvas.navigate(uidl.getBooleanAttribute(NAVIGATE_IDENTIFIER));
		} else if (uidl.getChildCount() == 0) {
			rootPanel.clear();
		} else {
			boolean canvasAlreadyAttached = canvas.isAttached();
			// Add subcomponents to rootPanel.
			for (int i = 0; i < uidl.getChildCount(); i++) {
				UIDL childUidl = uidl.getChildUIDL(i);
				String backgroundColor = childUidl.getStringAttribute(COMPONENT_BACKGROUND_COLOR_IDENTIFIER);

				UIDL componentUidl = childUidl.getChildUIDL(0);
				Paintable paintable = client.getPaintable(componentUidl);

				if ((COMPONENT_TAGNAME + CURRENT_COMPONENT_IDENTIFIER).equals(childUidl.getTag())) {
					// Current component received. Initialize.
					canvas.paperBackColor = uidl.getStringAttribute(PAPER_BACK_COLOR_IDENTIFIER);
					canvas.paperEdgeColor = uidl.getStringAttribute(PAPER_EDGE_COLOR_IDENTIFIER);
					canvas.cloneElements = new Element[uidl.getIntAttribute(CLONE_ELEMENT_COUNT_IDENTIFIER)];
					
					canvas.initialCornerX = uidl.getDoubleAttribute(PAPER_INITIAL_CORNER_X);
					canvas.initialCornerY = uidl.getDoubleAttribute(PAPER_INITIAL_CORNER_Y);
					canvas.initialHeight = uidl.getDoubleAttribute(PAPER_INITIAL_HEIGHT);
					canvas.initialWidth = uidl.getDoubleAttribute(PAPER_INITIAL_WIDTH);
					
					
					padding = uidl.getBooleanAttribute(PADDING_IDENTIFIER);

					previousComponent = null;
					currentComponent = null;
					nextComponent = null;
					rootPanel.clear();

					currentComponent = new PaintableWrapper(paintable, backgroundColor);
					currentComponent.show(true);
					rootPanel.add(currentComponent);
				} else if ((COMPONENT_TAGNAME + NEXT_COMPONENT_IDENTIFIER).equals(childUidl.getTag())) {
					nextComponent = new PaintableWrapper(paintable, backgroundColor);
					rootPanel.add(nextComponent);

					// Since this is the next component, display the canvas.
					canvas.switchSize(false);
					rootPanel.add(canvas);
					canvas.foldCorner(!canvasAlreadyAttached);
				} else if ((COMPONENT_TAGNAME + PREVIOUS_COMPONENT_IDENTIFIER).equals(childUidl.getTag())) {
					previousComponent = new PaintableWrapper(paintable, backgroundColor);
					rootPanel.add(previousComponent);

					// Since this is the previous component, display the back
					// button.
					rootPanel.add(backButton);
				}

				paintable.updateFromUIDL(componentUidl, client);
			}
		}
	}

	/**
	 * The canvas layer which handles the transition effect between
	 * subcomponents.
	 */
	private class PaperStackCanvas extends Canvas {
		private double initialCornerX = 5.0;
		private double initialCornerY = 10.0;
		private double initialWidth = 25.0;
		private double initialHeight = 35.0;

		private static final double DRAW_BOUNDARY = 20000.0;

		private static final String SHADOW_COLOR = "rgba(0, 0, 0, 0.3)";

		private final PaperStackAnimation animation = new PaperStackAnimation();

		private final Element cloneElementModel;
		public Element[] cloneElements;

		private Long dragStart = null;
		private boolean fullSize = false;

		private double canvasWidth;
		private double canvasHeight;

		public String paperBackColor;
		public String paperEdgeColor;

		public PaperStackCanvas() {
			setBackgroundColor(TRANSPARENT);

			Style style = getElement().getStyle();
			style.setProperty("right", "0px");
			style.setProperty("position", "absolute");

			// Create clone element model
			cloneElementModel = DOM.createDiv();
			Style cloneElementStyle = cloneElementModel.getStyle();
			cloneElementStyle.setProperty("bottom", "0px");
			cloneElementStyle.setProperty("width", "100%");
			cloneElementStyle.setProperty("height", "100%");
			cloneElementStyle.setProperty("position", "absolute");
			cloneElementStyle.setProperty("overflow", "hidden");

			sinkEvents(Event.MOUSEEVENTS);
		}

		public void navigate(final boolean forward) {
			if (!animation.running) {
				this.setFocus(true);
				if (forward && nextComponent != null) {
					switchSize(true);
					currentComponentToCloneElements();
					nextComponent.show(true);
					rootPanel.add(this);

					animation.showComponent = NEXT_COMPONENT_IDENTIFIER;
					animation.run(componentWidth - (initialWidth - initialCornerX), initialCornerY, -2.0 * componentWidth + initialCornerX, 2.0
							* componentHeight - initialCornerY);
				} else if (!forward && previousComponent != null) {
					switchSize(true);
					nextComponent = currentComponent;
					currentComponent = previousComponent;
					previousComponent = null;

					currentComponent.show(true);

					currentComponentToCloneElements();
					rootPanel.add(this);

					animation.showComponent = PREVIOUS_COMPONENT_IDENTIFIER;
					animation.run(-componentWidth, 2.0 * componentHeight, 2.0 * componentWidth - initialCornerX, -2.0 * componentHeight + initialCornerY);
				}
			}
		}

		@Override
		public void onBrowserEvent(final Event event) {
			event.preventDefault();
			if (!animation.running) {
				double clientX = event.getClientX() - VPaperStack.this.getAbsoluteLeft();
				double clientY = event.getClientY() - VPaperStack.this.getAbsoluteTop();

				switch (DOM.eventGetType(event)) {
				case Event.ONMOUSEMOVE:
					if (dragStart != null) {
						render(clientX, clientY);
					}
					break;
				case Event.ONMOUSEDOWN:
					grab(clientX, clientY);
					break;
				case Event.ONMOUSEOUT:
					if (isIE && (clientY > 0.0) && (clientX > 0.0) && (clientY < componentHeight) && (clientX < componentWidth)) {
						break;
					}
				case Event.ONMOUSEUP:
					release(clientX, clientY);
					break;
				default:
					break;
				}
			}
		}

		private void currentComponentToCloneElements() {
			int frontCloneElementIndex = cloneElements.length / 2;
			for (int i = 0; i < cloneElements.length; i++) {
				cloneElements[i] = (Element) cloneElementModel.cloneNode(false);
				if (i != frontCloneElementIndex) {
					cloneElements[i].appendChild(currentComponent.getElement().cloneNode(true));
					rootPanel.getElement().appendChild(cloneElements[i]);
				}
			}

			rootPanel.remove(currentComponent);
			// The frontmost clone element which wraps the actual
			// currentComponent is added lastly.
			cloneElements[frontCloneElementIndex].appendChild(currentComponent.getElement());
			rootPanel.getElement().appendChild(cloneElements[frontCloneElementIndex]);
		}

		private void grab(final double clientX, final double clientY) {
			if (dragStart == null) {
				this.setFocus(true);
				dragStart = new Date().getTime();

				switchSize(true);
				currentComponentToCloneElements();

				rootPanel.add(this);
				nextComponent.show(true);
				render(clientX, clientY);
			}
		}

		private void release(final double clientX, final double clientY) {
			if (dragStart != null) {
				double y = Math.min(Math.max(clientY, 0), canvasHeight);
				double x = Math.min(Math.max(clientX, 0), canvasWidth - 1);

				double xd = 0.0;
				double yd = 0.0;

				if ((dragStart + 500l > new Date().getTime()) || (y > getHeight() / 1.5) || (x < getWidth() / 3.0)) {
					// Reveal next subcomponent.
					animation.showComponent = NEXT_COMPONENT_IDENTIFIER;

					// Values needed to animate the corner to the same direction
					// where it was released.
					double componentDiagonalLength = Math.sqrt(Math.pow(componentHeight, 2.0) + Math.pow(componentWidth, 2.0));
					double componentDiagonalAngle = Math.atan(componentHeight / componentWidth);
					double alpha = Math.atan(y / (componentWidth - x));
					double beta = alpha - componentDiagonalAngle;
					double r = Math.cos(beta) * componentDiagonalLength;

					xd = componentWidth - x - 2.0 * r * Math.cos(alpha);
					yd = 2.0 * r * Math.sin(alpha) - y;
				} else {
					animation.showComponent = CURRENT_COMPONENT_IDENTIFIER;
					xd = getWidth() - initialWidth + initialCornerX - x;
					yd = initialCornerY - y;
				}
				dragStart = null;

				getElement().getStyle().setProperty("cursor", "default");

				animation.run(x, y, xd, yd);
			}
		}

		/**
		 * Moves the paper corner to the initial position.
		 */
		public final void foldCorner(boolean animate) {
			if (animate) {
				animation.showComponent = CURRENT_COMPONENT_IDENTIFIER;
				animation.run(initialWidth - 1.0, 1.0, initialCornerX - initialWidth, initialCornerY);
			} else {
				render(initialCornerX, initialCornerY);
			}
		}

		/**
		 * Changes the size of the canvas to either full-sized (the size of the
		 * whole component) or initial-sized (minimized to the right upper
		 * corner of the component).
		 * 
		 * @param fullSizeCanvas
		 *            should the canvas be maximized or not.
		 */
		public final void switchSize(final boolean fullSizeCanvas) {
			fullSize = fullSizeCanvas;
			canvasWidth = fullSize ? componentWidth : initialWidth;
			canvasHeight = fullSize ? componentHeight : initialHeight;

			setPixelSize((int) canvasWidth, (int) canvasHeight);
			setStrokeStyle(paperEdgeColor);

			if (!fullSize) {
				getElement().getStyle().setProperty("cursor", "pointer");
			}
		}

		/**
		 * Draws the paper corner and a shadow to the canvas. Resizes the clone
		 * elements to create an illusion of diagonal element borderline. The
		 * resulting sawtooth pattern is patched using the background color of
		 * the component.
		 * 
		 * @param cornerX
		 *            x-position of the corner.
		 * @param cornerY
		 *            y-position of the corner.
		 */
		public final void render(final double cornerX, final double cornerY) {
			double centerX = (cornerX + canvasWidth) / 2.0;
			double centerY = cornerY / 2.0;

			double cornerRight = canvasWidth - cornerX;

			double orthogonalTan = -cornerRight / cornerY;

			double rightY = centerY - orthogonalTan * (canvasWidth - centerX);
			rightY = rightY < DRAW_BOUNDARY ? rightY : DRAW_BOUNDARY;

			double leftX = centerX + centerY / orthogonalTan;
			leftX = leftX > -DRAW_BOUNDARY ? leftX : -DRAW_BOUNDARY;

			double rightXalt = canvasWidth - rightY * cornerRight / 4000.0;
			double candidate = leftX + 1.0;
			rightXalt = rightXalt > candidate ? rightXalt : candidate;

			double leftYalt = (canvasWidth - leftX) * cornerY / 4000.0;
			candidate = rightY - 1.0;
			leftYalt = leftYalt < candidate ? leftYalt : candidate;

			double controlPointLeftX = leftX + leftYalt / orthogonalTan;
			controlPointLeftX = controlPointLeftX > -DRAW_BOUNDARY ? controlPointLeftX : -DRAW_BOUNDARY;

			double controlPointRightY = rightY - (canvasWidth - rightXalt) * orthogonalTan;
			controlPointRightY = controlPointRightY < DRAW_BOUNDARY ? controlPointRightY : DRAW_BOUNDARY;

			double curveLeftX = 2 * controlPointLeftX - leftX;
			double curveRightY = 2 * controlPointRightY - rightY;

			double leftMiddleX = (leftX + cornerX) / 2.0;
			double leftMiddleY = (leftYalt + cornerY) / 2.0;

			double leftCurveMiddleX = (leftX + controlPointLeftX) / 2.0;
			double leftCurveMiddleY = leftYalt / 2.0;

			double rightMiddleX = (rightXalt + cornerX) / 2.0;
			double rightMiddleY = (rightY + cornerY) / 2.0;

			double rightCurveMiddleX = (canvasWidth + rightXalt) / 2.0;
			double rightCurveMiddleY = (controlPointRightY + rightY) / 2.0;

			double shadowOffset = (cornerY + cornerRight) / 8.0;

			double shadowControlPointRightX = 2.5 * rightXalt - (1.5 * canvasWidth);
			double shadowControlPointRightY = 0.2 * rightY + 0.8 * curveRightY;

			double shadowControlPointLeftX = 0.2 * leftX + 0.8 * curveLeftX;
			double shadowControlPointLeftY = leftYalt * 2.5;

			// Clear the canvas.
			clear();

			// For some odd reason rendering on IE slows down if this isn't
			// called here.
			getOffsetHeight();

			if (fullSize) {
				// Resize the clone elements and patch the resulting sawtooth
				// pattern.
				beginPath();
				moveTo(canvasWidth, curveRightY);
				quadraticCurveTo(canvasWidth, controlPointRightY, rightCurveMiddleX, rightCurveMiddleY);
				lineTo(leftCurveMiddleX, leftCurveMiddleY);
				quadraticCurveTo(controlPointLeftX, 0.0, curveLeftX - 1.0, 0.0);

				double previousX = curveLeftX > 0.0 ? curveLeftX : 0.0;
				cloneElements[0].getStyle().setProperty("width", previousX-- + "px");

				// Calculate temporary variables for optimal performance.
				double temp1 = componentHeight - leftCurveMiddleY;
				double temp2 = rightCurveMiddleY - leftCurveMiddleY;
				double temp3 = componentHeight + 1.0;
				double temp4 = rightCurveMiddleX - leftCurveMiddleX;

				for (int i = 1; i < cloneElements.length - 1; i++) {
					Style style = cloneElements[i].getStyle();
					double denominator2 = (i - 1.0) / (cloneElements.length - 3.0);
					double newHeight = temp1 - temp2 * denominator2;
					style.setProperty("height", (newHeight > 0.0 ? newHeight : 0.0) + "px");

					double y = temp3 - newHeight;
					lineTo(previousX, y);

					double newWidth = leftCurveMiddleX + temp4 * denominator2;
					previousX = newWidth > 0.0 ? newWidth : 0.0;
					style.setProperty("width", previousX-- + "px");
					lineTo(previousX, y);
				}

				lineTo(previousX, curveRightY + 1.0);
				cloneElements[cloneElements.length - 1].getStyle().setProperty("height",
						(curveRightY < componentHeight ? componentHeight - curveRightY : 0.0) + "px");
				closePath();

				setFillStyle(currentComponent.backgroundColor);
				fill();
			} else if (nextComponent != null) {
				// Fill the upper right corner with the background color of the
				// next subcomponent.
				beginPath();
				moveTo(canvasWidth, curveRightY);
				lineTo(canvasWidth, 0.0);
				lineTo(curveLeftX, 0.0);
				closePath();
				setFillStyle(nextComponent.backgroundColor);
				fill();
			}

			// Draw the shadow
			beginPath();
			moveTo(curveLeftX, 0.0);
			quadraticCurveTo(controlPointLeftX, 0.0, leftCurveMiddleX, leftCurveMiddleY);
			lineTo(rightCurveMiddleX, rightCurveMiddleY);
			quadraticCurveTo(canvasWidth, controlPointRightY, canvasWidth, curveRightY);
			quadraticCurveTo(shadowControlPointRightX, shadowControlPointRightY, cornerX - shadowOffset, cornerY + shadowOffset);
			quadraticCurveTo(shadowControlPointLeftX, shadowControlPointLeftY, curveLeftX, 0.0);
			closePath();

			setFillStyle(SHADOW_COLOR);
			fill();

			// Draw the paper.
			beginPath();
			moveTo(curveLeftX, 0.0);
			quadraticCurveTo(controlPointLeftX, 0.0, leftCurveMiddleX, leftCurveMiddleY);
			lineTo(rightCurveMiddleX, rightCurveMiddleY);
			quadraticCurveTo(canvasWidth, controlPointRightY, canvasWidth, curveRightY);
			quadraticCurveTo(canvasWidth, controlPointRightY, rightCurveMiddleX, rightCurveMiddleY);
			quadraticCurveTo(rightXalt, rightY, rightMiddleX, rightMiddleY);
			lineTo(cornerX, cornerY);
			lineTo(leftMiddleX, leftMiddleY);
			quadraticCurveTo(leftX, leftYalt, leftCurveMiddleX, leftCurveMiddleY);
			quadraticCurveTo(controlPointLeftX, 0.0, curveLeftX, 0.0);

			setFillStyle(paperBackColor);
			fill();
			stroke();
		}

		/**
		 * Animation class for the canvas.
		 */
		private class PaperStackAnimation extends Animation {
			private static final int ANIMATION_RUNTIME = 500;

			public boolean running;
			public int showComponent;

			private double startX;
			private double startY;
			private double xd;
			private double yd;

			@Override
			protected void onComplete() {
				rootPanel.clear();

				if (showComponent == CURRENT_COMPONENT_IDENTIFIER) {
					nextComponent.show(false);
					rootPanel.add(nextComponent);
					rootPanel.add(currentComponent);
					currentComponent.show(true);
					if (previousComponent != null) {
						previousComponent.show(false);
						rootPanel.add(previousComponent);
						rootPanel.add(backButton);
					}
					canvas.switchSize(false);
					rootPanel.add(canvas);
					canvas.render(initialCornerX, initialCornerY);
					client.updateVariable(paintableId, PAGECHANGE_EVENT_IDENTIFIER, CURRENT_COMPONENT_IDENTIFIER, true);
				} else if (showComponent == NEXT_COMPONENT_IDENTIFIER) {
					previousComponent = currentComponent;
					currentComponent = nextComponent;
					nextComponent = null;
					previousComponent.show(false);
					rootPanel.add(previousComponent);
					currentComponent.show(true);
					rootPanel.add(currentComponent);
					rootPanel.add(backButton);
					client.updateVariable(paintableId, PAGECHANGE_EVENT_IDENTIFIER, NEXT_COMPONENT_IDENTIFIER, true);
				} else if (showComponent == PREVIOUS_COMPONENT_IDENTIFIER) {
					nextComponent.show(false);
					rootPanel.add(nextComponent);
					currentComponent.show(true);
					rootPanel.add(currentComponent);
					canvas.switchSize(false);
					rootPanel.add(canvas);
					canvas.foldCorner(false);
					client.updateVariable(paintableId, PAGECHANGE_EVENT_IDENTIFIER, PREVIOUS_COMPONENT_IDENTIFIER, true);
				}
				running = false;
			}

			/**
			 * Animates the canvas.
			 * 
			 * @param animStartX
			 *            The start X-position of the paper corner.
			 * @param animStartY
			 *            The start Y-position of the paper corner.
			 * @param animXd
			 *            Corner X-change during the animation.
			 * @param animYd
			 *            Corner Y-change during the animation.
			 */
			public void run(final double animStartX, final double animStartY, final double animXd, final double animYd) {
				startX = animStartX;
				startY = animStartY;
				xd = animXd;
				yd = animYd;
				running = true;
				run(ANIMATION_RUNTIME);
			}

			@Override
			protected void onUpdate(final double progress) {
				canvas.render(startX + xd * progress, startY + yd * progress);
			}

		}
	}

	/**
	 * Wrapper class for a Paintable.
	 */
	private class PaintableWrapper extends SimplePanel {

		private final String backgroundColor;

		public PaintableWrapper(final Paintable paintable, final String wrapperBackgroundColor) {
			backgroundColor = wrapperBackgroundColor;
			Style style = PaintableWrapper.this.getElement().getStyle();
			style.setProperty("backgroundColor", backgroundColor);
			style.setProperty("bottom", "0px");
			style.setProperty("position", "absolute");
			style.setProperty("overflow", "hidden");
			style.setProperty("padding", (padding ? PADDING_WIDTH : 0.0) + "px");

			setWidget((Widget) paintable);
			double paddingSubtraction = padding ? (PADDING_WIDTH * 2.0) : 0.0;
			setPixelSize((int) (componentWidth - paddingSubtraction), (int) (componentHeight - paddingSubtraction));

			show(false);
		}

		/**
		 * Hides/shows this PaintableWrapper
		 * 
		 * @param show
		 */
		public void show(final boolean show) {
			getElement().getStyle().setProperty("visibility", show ? "visible" : "hidden");
		}

		/**
		 * Resizes the PaintableWrapper and handles the Paintable's possible
		 * relative size.
		 */
		public void refreshSize() {
			double paddingSubtraction = padding ? (PADDING_WIDTH * 2.0) : 0.0;
			setPixelSize((int) (componentWidth - paddingSubtraction), (int) (componentHeight - paddingSubtraction));
			client.handleComponentRelativeSize(getWidget());
		}
	}

	@Override
	public RenderSpace getAllocatedSpace(Widget child) {
		double paddingSubtraction = padding ? (PADDING_WIDTH * 2.0) : 0.0;
		return new RenderSpace((int) (componentWidth - paddingSubtraction), (int) (componentHeight - paddingSubtraction));
	}

	@Override
	public boolean hasChildComponent(Widget component) {
		for (PaintableWrapper component2 : Arrays.asList(currentComponent, nextComponent, previousComponent)) {
			if (component2 != null && component2.getWidget().equals(component)) {
				return true;
			}
		}
		return false;
	}

	@Override
	public void replaceChildComponent(Widget oldComponent, Widget newComponent) {
		for (PaintableWrapper component : Arrays.asList(currentComponent, nextComponent, previousComponent)) {
			if (component != null && component.getWidget().equals(oldComponent)) {
				component.setWidget(newComponent);
				return;
			}
		}
	}

	@Override
	public boolean requestLayout(Set<Paintable> children) {
		return true;
	}

	@Override
	public void updateCaption(Paintable component, UIDL uidl) {
		// Captions not supported
	}

}
